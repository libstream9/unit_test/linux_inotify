#include <stream9/linux/inotify/inotify.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

#include <stream9/filesystem/remove_recursive.hpp>
#include <stream9/filesystem/temporary_directory.hpp>
#include <stream9/json.hpp>
#include <stream9/json/algorithm.hpp>
#include <stream9/linux/chmod.hpp>
#include <stream9/linux/mkdir.hpp>
#include <stream9/linux/rename.hpp>
#include <stream9/linux/rmdir.hpp>
#include <stream9/linux/unlink.hpp>
#include <stream9/path/concat.hpp> // operator/

#include <concepts>
#include <fstream>

namespace testing {

using stream9::path::operator/;

BOOST_AUTO_TEST_SUITE(inotify_)

    BOOST_AUTO_TEST_CASE(concept_)
    {
        static_assert(std::default_initializable<lx::inotify>);
        static_assert(!std::copyable<lx::inotify>);
        static_assert(std::movable<lx::inotify>);
    }

    BOOST_AUTO_TEST_CASE(default_constructor_)
    {
        lx::inotify ino;
    }

    BOOST_AUTO_TEST_CASE(create_directory_)
    {
        lx::inotify ino { IN_NONBLOCK };
        fs::temporary_directory dir;
        char buf[1024];

        ino.add_watch(dir.path(), IN_ALL_EVENTS);

        lx::mkdir(dir.path() / "foo");

        auto events = ino.read(buf);

        auto expected = json::array {
            {
                { "wd", 1 },
                { "mask", "IN_CREATE | IN_ISDIR" },
                { "name", "foo" },
            },
        };

        BOOST_TEST(expected == json::value_from(events));
    }

    BOOST_AUTO_TEST_CASE(delete_file_)
    {
        lx::inotify ino { IN_NONBLOCK };
        fs::temporary_directory dir;
        char buf[1024];

        {
            std::ofstream ofs { dir.path() / "foo" };
        }

        ino.add_watch(dir.path(), IN_ALL_EVENTS);

        lx::unlink(dir.path() / "foo");

        auto events = ino.read(buf);

        auto expected = json::array {
            {
                { "wd", 1 },
                { "mask", "IN_DELETE" },
                { "name", "foo" },
            },
        };

        BOOST_TEST(expected == json::value_from(events));
    }

    BOOST_AUTO_TEST_CASE(delete_directory_)
    {
        lx::inotify ino { IN_NONBLOCK };
        fs::temporary_directory dir;
        char buf[1024];

        {
            lx::mkdir(dir.path() / "foo");
        }

        ino.add_watch(dir.path(), IN_ALL_EVENTS);

        BOOST_TEST(ino.available_data_size() == 0);

        lx::rmdir(dir.path() / "foo");

        BOOST_TEST(ino.available_data_size() != 0);

        auto events = ino.read(buf);

        auto expected = json::array {
            {
                { "wd", 1 },
                { "mask", "IN_DELETE | IN_ISDIR" },
                { "name", "foo" },
            },
        };

        BOOST_TEST(expected == json::value_from(events));
    }

    BOOST_AUTO_TEST_CASE(access_file_)
    {
        lx::inotify ino { IN_NONBLOCK };
        fs::temporary_directory dir;
        char buf[1024];

        std::ofstream ofs { dir.path() / "foo" };
        ofs << "foo";
        ofs.close();

        std::ifstream ifs { dir.path() / "foo" };

        ino.add_watch(dir.path(), IN_ALL_EVENTS);

        BOOST_TEST(ino.available_data_size() == 0);

        std::string s;
        std::getline(ifs, s);
        ifs.close();

        BOOST_TEST(ino.available_data_size() != 0);

        auto events = ino.read(buf);

        auto expected = json::array {
            {
                { "wd", 1 },
                { "mask", "IN_ACCESS" },
                { "name", "foo" },
            },
            {
                { "wd", 1 },
                { "mask", "IN_CLOSE_NOWRITE" },
                { "name", "foo" },
            },
        };

        BOOST_TEST(expected == json::value_from(events));
    }

    BOOST_AUTO_TEST_CASE(modify_file_)
    {
        lx::inotify ino { IN_NONBLOCK };
        fs::temporary_directory dir;
        char buf[1024];

        std::ofstream ofs { dir.path() / "foo" };

        ino.add_watch(dir.path(), IN_ALL_EVENTS);

        BOOST_TEST(ino.available_data_size() == 0);

        ofs << "foo" << std::flush;
        ofs.close();

        BOOST_TEST(ino.available_data_size() != 0);

        auto events = ino.read(buf);

        auto expected = json::array {
            {
                { "wd", 1 },
                { "mask", "IN_MODIFY" },
                { "name", "foo" },
            },
            {
                { "wd", 1 },
                { "mask", "IN_CLOSE_WRITE" },
                { "name", "foo" },
            },
        };

        BOOST_TEST(expected == json::value_from(events));
    }

    BOOST_AUTO_TEST_CASE(modify_file_attribute_)
    {
        lx::inotify ino { IN_NONBLOCK };
        fs::temporary_directory dir;
        char buf[1024];

        std::ofstream ofs { dir.path() / "foo" };

        ino.add_watch(dir.path(), IN_ALL_EVENTS);

        BOOST_TEST(ino.available_data_size() == 0);

        lx::chmod(dir.path() / "foo", 0700);


        BOOST_TEST(ino.available_data_size() != 0);

        auto events = ino.read(buf);

        auto expected = json::array {
            {
                { "wd", 1 },
                { "mask", "IN_ATTRIB" },
                { "name", "foo" },
            },
        };

        BOOST_TEST(expected == json::value_from(events));
    }

    BOOST_AUTO_TEST_CASE(rename_file_)
    {
        lx::inotify ino { IN_NONBLOCK };
        fs::temporary_directory dir;
        char buf[1024];

        std::ofstream ofs { dir.path() / "foo" };

        ino.add_watch(dir.path(), IN_ALL_EVENTS);

        BOOST_TEST(ino.available_data_size() == 0);

        lx::rename(dir.path() / "foo", dir.path() / "bar");

        BOOST_TEST(ino.available_data_size() != 0);

        auto events = ino.read(buf);

        auto expected = json::array {
            {
                { "wd", 1 },
                { "mask", "IN_MOVED_FROM" },
                //{ "cookie", 200670 },
                { "name", "foo" },
            },
            {
                { "wd", 1 },
                { "mask", "IN_MOVED_TO" },
                //{ "cookie", 200670 },
                { "name", "bar" },
            },
        };
        using namespace json::literals;

        auto v = json::value_from(events);

        auto o_cookie = json::find(v, "/0/cookie"_jp);
        BOOST_TEST((o_cookie && *o_cookie != 0));
        json::erase(v, "/0/cookie"_jp);

        o_cookie = json::find(v, "/1/cookie"_jp);
        BOOST_TEST((o_cookie && *o_cookie != 0));
        json::erase(v, "/1/cookie"_jp);

        BOOST_TEST(expected == v);
    }

    BOOST_AUTO_TEST_CASE(delete_watched_directory_)
    {
        lx::inotify ino { IN_NONBLOCK };
        fs::temporary_directory dir;
        char buf[1024];

        ino.add_watch(dir.path(), IN_ALL_EVENTS);

        BOOST_TEST(ino.available_data_size() == 0);

        lx::rmdir(dir.path());
        dir.set_auto_delete(false);

        BOOST_TEST(ino.available_data_size() != 0);

        auto events = ino.read(buf);

        auto expected = json::array {
            {
                { "wd", 1 },
                { "mask", "IN_DELETE_SELF" },
            },
            {
                { "wd", 1 },
                { "mask", "IN_IGNORED" },
            },
        };

        BOOST_TEST(expected == json::value_from(events));
    }

BOOST_AUTO_TEST_SUITE_END() // inotify_

} // namespace testing
