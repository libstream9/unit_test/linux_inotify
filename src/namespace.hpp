#ifndef STREAM9_LINUX_INOTIFY_TEST_SRC_NAMESPACE_HPP
#define STREAM9_LINUX_INOTIFY_TEST_SRC_NAMESPACE_HPP

namespace stream9::linux {}
namespace stream9::filesystem {}
namespace stream9::json {}

namespace testing {

namespace lx { using namespace stream9::linux; }
namespace fs { using namespace stream9::filesystem; }
namespace json { using namespace stream9::json; }

} // namespace testing

#endif // STREAM9_LINUX_INOTIFY_TEST_SRC_NAMESPACE_HPP
